﻿#include <stdio.h>
#include <windows.h>
#include <chrono>
#include <time.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	int  n, j, s = 0, tmp;
	float arr[1000];
	printf("Введіть розмірність масиву: "); scanf_s("%d", &n);
	while ((double)pow(2, s) - 1 <= n)														//Пошук найбільшого значення s, яке задовільняє формулу
		s++;
	int inc = (double)pow(2, s) - 1;
	printf("\nМасив до сортування: [ ");
	for (int i = 0; i < n; i++) {
		arr[i] = 10 + rand() % (101-10);
		printf("%.2f ", arr[i]);
	}
	printf("]");

	auto begin = std::chrono::steady_clock::now();
	while (s >= -1)
	{
		for (int i = 0; i < (n - inc); i++)													//Поділ масиву на групи для подальшого сортування
		{
			j = i;
			while (j >= 0 && arr[j] > arr[j + inc])											//Сортування груп методом вставки
			{
				tmp = arr[j];
				arr[j] = arr[j + inc];
				arr[j + inc] = tmp;
				j -= inc;
			}
		}
		s--;																				//Перерахування формули приросту з меншим s для сортування меншої кількості груп
		inc = (double)pow(2, s) + 1;
	}
	auto end = std::chrono::steady_clock::now();


	auto result = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
	printf("\n\nМасив після сортування: [ ");
	for (int i = 0; i < n; i++)
		printf("%.2f ", arr[i]);
	printf("]\n");
	printf("\nЧас, витрачений на сортування методом Шелла: %lld ns.", result.count());
	return 0;
}