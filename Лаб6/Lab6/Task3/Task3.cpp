﻿#include <stdio.h>
#include <time.h>
#include <chrono>
#include <windows.h>

void countingSort(signed char* arr, int max, int min, int n) {
	signed char temp[10000] = { 0 };									//Додатковий масив для діапазону чисел
	int j = 0;
	for (int i = 0; i < n; i++)											//В допоміжний масив, де індекс = число результуючого 
		temp[arr[i] - min]++;											//масиву записується кількість чисел шляхом додавання 1 при кожній зустрічі
	for (int i = min; i <= max; i++)
		while (temp[i - min]-- > 0)
		{
			arr[j] = i;													//В результуючий масив записуються числа
			j++;
		}
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	signed char arr[1000], max = -15, min = -10;
	int n;
	printf("Введіть розмірність масиву: "); scanf_s("%d", &n);
	printf("\nМасив до сортування: [ ");
	for (int i = 0; i < n; i++)
	{
		arr[i] = rand() % (-4) - 10;
		if (arr[i] < min)
			min = arr[i];									//Знаходження мінімального порогу діапазону чисел
		if (arr[i] > max)
			max = arr[i];											//Знаходження максимального порогу діапазону чисел
		printf("%d ", arr[i]);
	}
	printf("]");




	auto begin = std::chrono::steady_clock::now();

	countingSort(arr, max, min, n);

	auto end = std::chrono::steady_clock::now();
	auto result = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
	printf("\n\nМасив після сортування: [ ");
	for (int i = 0; i < n; i++)
		printf("%d ", arr[i]);
	printf("]\n");
	printf("\nЧас, витрачений на сортування підрахунком: %lld ns.", result.count());
	return 0;
}