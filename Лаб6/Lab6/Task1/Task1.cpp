﻿#include <stdio.h>
#include <windows.h>
#include <chrono>
#include <time.h>

void ConvertToHeap(double* arr, int n, int i) {
	double temp;
	int largest = i;
	int left = 2 * i + 1;
	int right = 2 * i + 2;
	if (left < n && arr[left] > arr[largest])			//Якщо лівий нащадок > корінь
		largest = left;
	if (right < n && arr[right] > arr[largest])			//Якщо правий нащадок > корінь
		largest = right;
	if (largest != i)									//Якщо найбільший елемент не є коренем
	{
		temp = arr[i];
		arr[i] = arr[largest];
		arr[largest] = temp;
		ConvertToHeap(arr, n, largest);					//Рекурсивно перетворюємо в кучу піддерево з нащадків найбільшого елемента
	}
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	double arr[1000], temp;
	int i, n, x = 0;
	printf("Введіть розмірність масиву: "); scanf_s("%d", &n);
	printf("\nМасив до сортування: [ ");
	for (int i = 0; i < n; i++)
	{
		arr[i] = -200 + rand() % (2010 - 200) / 1.1;
		printf("%lf ", arr[i]);
	}
	printf("]");


	auto begin = std::chrono::steady_clock::now();
	for (int i = n / 2 - 1; i >= 0; i--) //Перетворення масиву в кучу
		ConvertToHeap(arr, n, i);
	for (int i = n - 1; i >= 0; i--) //Перегляд кучі з кінця
	{
		temp = arr[0];
		arr[0] = arr[i];
		arr[i] = temp; //Зміна місцями першого та і-го з кінця елемента кучі
		ConvertToHeap(arr, i, 0); //Перетворення в кучу масиву без впорядкованих елементів в кінці
	}
	auto end = std::chrono::steady_clock::now();


	auto result = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
	printf("\n\nМасив після сортування: [ ");
	for (int i = 0; i < n; i++)
		printf("%lf ", arr[i]);
	printf("]\n");
	printf("\nЧас, витрачений на пірамідальне сортування: %lld ns.", result.count());
	return 0;
}