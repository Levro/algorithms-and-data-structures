﻿#include <stdio.h>
#include <windows.h>

union {
	float a;
	struct binaryCode {
		unsigned short bit1 : 1; unsigned short bit2 : 1;
		unsigned short bit3 : 1; unsigned short bit4 : 1;
		unsigned short bit5 : 1; unsigned short bit6 : 1;
		unsigned short bit7 : 1; unsigned short bit8 : 1;
		unsigned short bit9 : 1; unsigned short bit10 : 1;
		unsigned short bit11 : 1; unsigned short bit12 : 1;
		unsigned short bit13 : 1; unsigned short bit14 : 1;
		unsigned short bit15 : 1; unsigned short bit16 : 1;
		unsigned short bit17 : 1; unsigned short bit18 : 1;
		unsigned short bit19 : 1; unsigned short bit20 : 1;
		unsigned short bit21 : 1; unsigned short bit22 : 1;
		unsigned short bit23 : 1; unsigned short bit24 : 1;
		unsigned short bit25 : 1; unsigned short bit26 : 1;
		unsigned short bit27 : 1; unsigned short bit28 : 1;
		unsigned short bit29 : 1; unsigned short bit30 : 1;
		unsigned short bit31 : 1; unsigned short bit32 : 1;
	}bits;
	struct
	{
		unsigned char byte1;
		unsigned char byte2;
		unsigned char byte3;
		unsigned char byte4;
	}bytes;
}unValue;
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Введіть число: ");
	scanf_s("%f", &unValue.a);
	printf("Двійковий код:\n");
	printf("%d %d%d%d%d%d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d%d %d%d%d",
		unValue.bits.bit32, unValue.bits.bit31, unValue.bits.bit30, unValue.bits.bit29, unValue.bits.bit28, 
		unValue.bits.bit27, unValue.bits.bit26, unValue.bits.bit25, unValue.bits.bit24, unValue.bits.bit23, 
		unValue.bits.bit22, unValue.bits.bit21, unValue.bits.bit20, unValue.bits.bit19, unValue.bits.bit18, 
		unValue.bits.bit17, unValue.bits.bit16, unValue.bits.bit15, unValue.bits.bit14, unValue.bits.bit13,
		unValue.bits.bit12, unValue.bits.bit11, unValue.bits.bit10, unValue.bits.bit9, unValue.bits.bit8, 
		unValue.bits.bit7, unValue.bits.bit6, unValue.bits.bit5, unValue.bits.bit4, unValue.bits.bit3, 
		unValue.bits.bit2, unValue.bits.bit1);

	printf("\nЗнак введеного числа: ");
	if (unValue.bits.bit32 == 1)
		printf("[-]");
	else
		printf("[+]");

	printf("\nХарактеристика: %d%d%d%d%d%d%d%d", unValue.bits.bit31, unValue.bits.bit30,
		unValue.bits.bit29, unValue.bits.bit28, unValue.bits.bit27, unValue.bits.bit26, 
		unValue.bits.bit25, unValue.bits.bit24);

	printf("\nМантиса: |%d%d%d%d|%d%d%d%d|%d%d%d%d|%d%d%d%d|%d%d%d%d|%d%d%d",
		unValue.bits.bit23, unValue.bits.bit22, unValue.bits.bit21, unValue.bits.bit20, unValue.bits.bit19, unValue.bits.bit18, 
		unValue.bits.bit17, unValue.bits.bit16, unValue.bits.bit15, unValue.bits.bit14, unValue.bits.bit13, unValue.bits.bit12,
		unValue.bits.bit11, unValue.bits.bit10, unValue.bits.bit9, unValue.bits.bit8, unValue.bits.bit7, unValue.bits.bit6, 
		unValue.bits.bit5, unValue.bits.bit4, unValue.bits.bit3, unValue.bits.bit2, unValue.bits.bit1);

	printf("\nПобайтове значення: %x %x %x %x", unValue.bytes.byte4, unValue.bytes.byte3, unValue.bytes.byte2, unValue.bytes.byte1);

	printf("\nСтруктура займає: %d байти.", sizeof(unValue.bits));
	return 0;
}