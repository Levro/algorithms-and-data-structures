﻿#include <stdio.h>
#include <windows.h>

union
{
	signed short a;
	struct
	{
		signed short value : 15;
		signed short sign : 1;
	}bytes;
}unValue;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Використання об'єднання та структури:\n");
	printf("Введіть ваше число: ");
	signed short x;
	scanf_s("%hd", &x); // input value
	unValue.a = x;
	if ((unValue.bytes.sign == 0) && (unValue.bytes.value != 0))
		printf("Знак: [+];\nЗначення: %d", unValue.bytes.value);
	else if ((unValue.bytes.sign != 0) && (unValue.bytes.value != 0))
	{
		printf("Знак: [-];\nЗначення: %d", unValue.bytes.value);
		unValue.bytes.value *= -1;
	}
	else 
		printf("Ви ввели число 0, яке не має знаку!");
	unValue.a = (x >> 15);
	printf("\nВикористання побітових операцій:\n");
	if (x != 0)
		unValue.a ? printf("Знак: [-]\nЗначення: %d", -x) : printf("Знак: [+]\nЗначення: %d", x);
	else 
		printf("Ви ввели число 0, яке не має знаку!");
	return 0;
}