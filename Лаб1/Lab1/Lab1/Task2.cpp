﻿#include <stdio.h>
#include <windows.h>
#include <time.h>

struct RealTimeStruct
{
	unsigned short year : 7; // max 99 => 1100011 - 7
	unsigned short month : 4; // max 12 => 1100 - 4 
	unsigned short day : 5; // max 31 => 11111 - 5 
	unsigned short seconds : 6; // max 60 => 111100 - 6 
	unsigned short minutes : 6; // max 60 => 111100 - 6
	unsigned short hours : 5; // max 24 => 11000 - 5
	unsigned short weekday : 3; // max 7 => 111 - 3
};

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	RealTimeStruct date;
	unsigned short scan;

	//INPUT
	printf("Введіть актуальний час:\nГодини (0-24): ");
	scanf_s("%hu", &scan); // input in hours
	date.hours = scan;
	printf("Хвилини (0-60): "); 
	scanf_s("%hu", &scan); // input in minutes
	date.minutes = scan;
	printf("Секунди (0-60): ");
	scanf_s("%hu", &scan);  // input in seconds
	date.seconds = scan;
	printf("Введіть актуальну дату:");
	printf("\nРік: 20");
	scanf_s("%hu", &scan); // input in year
	date.year = scan; 
	printf("Місяць (0-12): ");
	scanf_s("%hu", &scan); // input in month
	date.month = scan;
	printf("День (0-31): ");  
	scanf_s("%hu", &scan); // input in day
	date.day = scan;
	printf("День тижня (1-7): "); 
	scanf_s("%hu", &scan); // input in weekday
	date.weekday = scan;

	//DATE
	printf("----Дата і час----\nДата ");
	if (date.day < 10)
		printf("0%d", date.day);
	else 
		printf("%d", date.day);

	if (date.month < 10)
		printf(".0%d", date.month);
	else
		printf(".%d", date.month);
	if (date.year < 10)
		printf(".200%d", date.year);
	else
		printf(".20%d", date.year);

	//WEEKDAY
	printf("\nДень тижня: ");
	if (date.weekday == 1) 
		printf("Понеділок");
	else if (date.weekday == 2) 
		printf("Вівторок");
	else if (date.weekday == 3) 
		printf("Середа");
	else if (date.weekday == 4) 
		printf("Четвер");
	else if (date.weekday == 5) 
		printf("П'ятниця");
	else if (date.weekday == 6) 
		printf("Субота");
	else if(date.weekday == 7)
		printf("Неділя");

	//HOURS
	printf("\nЧас ");
	if (date.hours < 10)
		printf("0%d", date.hours);
	else
		printf("%d", date.hours);
	if (date.minutes < 10)
		printf(":0%d", date.minutes);
	else
		printf(":%d", date.minutes);
	if (date.seconds < 10)
		printf(":0%d", date.seconds);
	else
		printf(":%d", date.seconds);
	printf("\n------------------");
	printf("\nСтруктура займає % d байт", sizeof(date));
	printf("\nСтруктура [tm] бібліотеки <time.h> займає: %d байт\n\n", sizeof(tm));
	return 0;
}


