﻿#include <stdio.h>
#include <windows.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	signed char a = 5 + 127;
	signed char b = 2 - 3;
	signed char c = -120 - 34;
	signed char d = (unsigned char)(-5);
	signed char e = 56 & 38;
	signed char f = 56 | 38;

	printf("a) 5+127\nРезультат: %d", a);
	printf("\n 0000 0101\n+0111 1111");
	printf("\n 1000 0100 = 132");

	printf("\n\nб) 2-3\nРезультат: %d", b);
	printf("\n 0000 0010\n-0000 0011");
	printf("\n 1111 1111 = 255");

	printf("\n\nв) -120-34\nРезультат: %d", c);
	printf("\n 1000 1000\n-0010 0010");
	printf("\n 0110 0110 = -154");

	printf("\n\nг) (unsigned char)(-5)\nРезультат: %d", d);

	printf("\n\nд) 56 & 38\nРезультат: %d", e);
	printf("\n 0011 1000\n&0010 0110");
	printf("\n 0010 0000 = 32");

	printf("\n\nе) 56 | 38\nРезультат: %d", f);
	printf("\n 0011 1000\n|0010 0110");
	printf("\n 0011 1110 = 62");


	return 0;
}