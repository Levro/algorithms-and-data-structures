﻿#include <stdio.h>
#include <windows.h>
#include <math.h>

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    unsigned long long a, c, n, m;
    n = 50000;
    unsigned int* arr1 = (unsigned int*)malloc(sizeof(unsigned int) * n);
    a = 48271;
    c = 0;
    m = pow(2, 31) - 1;
    int vibor = 0;
    printf("1. Вивести 50.000 елементів масиву\n2. Вивести тільки інформацію\n: ");
    scanf_s("%d", &vibor);

    for (int i = 0; i < n; i++)
    {
        arr1[i + 1] = (a * arr1[i] + c) % m;
        arr1[i] = arr1[i] % (300); 
        if (vibor == 1) printf("array[%d] = %d\n", i + 1, arr1[i]);
    }

    unsigned char arr2[300]; int freq;
    printf("Частота інтервалів появи випадкових величин:\n");
    for (int i = 0; i < 300; i++)
    {
        freq = 0;
        for (int j = 0; j < 49999; j++) {
            if (arr1[j] == i) freq++;
        }   
        arr2[i] = freq;
        printf("\nЧисло %d зустрічається %d разів", i, arr2[i]);
    }

    float statisticProb[300];
    printf("\nCтатистична імовірність появи випадкових величин:\n");
    float mathExpect = 0, dispersion = 0;
    for (int i = 0; i < 300; i++)
    {
        statisticProb[i] = ((float)arr2[i] / n);       
        printf("P[%d] = %.5f\n", i, statisticProb[i]);
        mathExpect += i * statisticProb[i];    
        dispersion += pow((i - mathExpect), 2) * statisticProb[i];
    }
    float standartDeviation = sqrt(dispersion);
    printf("Математичне сподівання випадкових величин = %.3f", mathExpect);
    printf("\nДисперсія випадкових величин = %.3f", dispersion);
    printf("\nСередньоквадратичне відхилення випадкових величин = %.3f\n", standartDeviation);
    return 0;
}