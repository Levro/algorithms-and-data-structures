﻿#include <stdio.h>
#include <windows.h>
#include <chrono>

void arraySort(int* arr, int length)
{
	int tmp;
	bool check;
	do
	{
		check = false;
		for (int i = 0; i < length; i++)
		{
			if (arr[i] < arr[i + 1])
			{
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				check = true;
			}
		}
	} while (check);
}
int main()
{
	srand(time(0));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	bool menu = true;
	int m[1023], length;
	while (menu) {
		
		printf("Введіть кіл-сть елементів масиву:");
		scanf_s("%d", &length);
		if (length >= 1024) {
			printf("Максимальний розмір масиву - 1023 елементa\n");
			continue;
		}
		for (int i = 0; i < length; i++)
		{
			m[i] = rand();
			printf("\narr[%d] = %d", i, m[i]);
		}
		auto begin = std::chrono::steady_clock::now();
		arraySort(m, length);
		auto end = std::chrono::steady_clock::now();
		auto resultTime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
		printf("\nВідсортований масив:");
		for (int i = 0; i < length; i++) printf("\narr[%d] = %d", i, m[i]);
		printf("\nЧас, витрачений на роботу функції: %lld ns\n", resultTime.count());
		//menu = false;
	}
	return 0;
}