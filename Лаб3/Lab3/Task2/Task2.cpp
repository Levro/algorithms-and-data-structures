﻿#include <stdio.h>
#include <windows.h>
#include <chrono>

int findMax(unsigned int m[], unsigned int length)
{
	unsigned int max;
	for (int i = 0; i < length; i++)
		if (i == 0 || m[i] > max) max = m[i];
	return max;
}


int main()
{
	srand(time(0));
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	bool menu = true;
	unsigned int m[40], length, res;
	
	while (menu) {
		printf("Введіть кіл-сть елементів масиву:");
		scanf_s("%d", &length);
		if (length > 40) {
			printf("Максимальний розмір масиву - 40 елементів\n");
			continue;
		}
		auto begin = std::chrono::steady_clock::now();
		for (int i = 0; i < length; i++) m[i] = rand();
		res = findMax(m, length);
		auto end = std::chrono::steady_clock::now();
		auto resultTime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
		printf("Максимальний можливий елемент масиву = %d\n", res);
		printf("Час, витрачений на роботу функції: %lld ns\n", resultTime.count());
		//menu = false;
	}
	return 0;
}