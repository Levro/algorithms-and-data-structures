﻿#include <stdio.h>
#include<windows.h>
#include <math.h>

double factorial(int n)
{
	if (n == 0)
		return 1;
	return n * factorial(n - 1);
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double res = 0;
	printf("Функція f(n) = n:\n");
	for (int i = 1; i <= 50; i++)
	{
		res = i;
		printf("f(%d) = %.f", i, res);
		if (i % 5 == 0) printf("\n");
	}
	printf("\n\nФункція f(n) = log(n):\n");
	for (int i = 1; i <= 50; i++)
	{
		res = log(i);
		printf("f(%d) = %.2f", i, res);
		if (i % 5 == 0) printf("\n");
	}
	printf("\n\nФункція f(n) = n*log(n):\n");
	for (int i = 1; i <= 50; i++)
	{
		res = i * log(i);
		printf("f(%d) = %.2f", i, res);
		if (i % 5 == 0) printf("\n");
	}
	printf("\n\nФункція f(n) = n^2:\n");
	for (int i = 1; i <= 50; i++)
	{
		res = i * i;
		printf("f(%d) = %.f", i, res);
		if (i % 5 == 0) printf("\n");
	}
	printf("\n\nФункція f(n) = 2^n:\n");
	for (int i = 1; i <= 50; i++)
	{
		res = pow(2, i);
		printf("f(%d) = %.f", i, res);
		if (i % 5 == 0) printf("\n");
	}
	printf("\n\nФункція f(n) = n!:\n");
	for (int i = 1; i <= 50; i++)
	{
		res = factorial(i);
		printf("f(%d) = %.f\n", i, res);
	}
}