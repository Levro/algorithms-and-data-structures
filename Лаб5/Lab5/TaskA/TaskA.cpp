﻿#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <chrono>

struct Element
{
	int value;
	struct Element* next;
	struct Element* previous;
};
struct List
{
	struct Element* head;
	struct Element* tail;
	size_t leng;
};

List* newList(void) {
	List* list = (List*)malloc(sizeof(List));
	if (list) {
		(*list).leng = 0;
		(*list).head = (*list).tail = NULL;
	}
	return list;
}
bool ListCheck(List* list)
{
	return (((*list).head == NULL) || ((*list).tail == NULL));
}
void printList(List* list)
{
	Element* out = (Element*)malloc(sizeof(Element));
	out = (*list).head;
	printf("Список:\n[ _ ");
	for (int i = 0; i < (*list).leng; i++)
	{
		printf("%d _ ", (*out).value);
		out = (*out).next;
	}
	printf("]\n");
}
void newHead(List* list, int* data)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = *data;
	(*elem).next = (*list).head;
	(*elem).previous = NULL;
	if (!ListCheck(list))
		(*(*list).head).previous = elem;
	else
		(*list).tail = elem;
	(*list).head = elem;
	(*list).leng++;
}
void newTail(List* list, int* data)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = *data;
	(*(*list).tail).next = elem;
	(*list).tail = elem;
	(*list).leng++;
}
void newElem(List* list, int* data, int index)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = *data;
	Element* tmp = (Element*)malloc(sizeof(Element));
	tmp = (*list).head;
	for (int i = 1; i < index; i++)
		tmp = (*tmp).next;
	(*elem).next = (*tmp).next;
	(*tmp).next = elem;
	(*list).leng++;
}
void SelectSort(List* list)
{
	Element* tmp = (Element*)malloc(sizeof(Element));
	Element* tmpLeft = (Element*)malloc(sizeof(Element));
	Element* tmpRight = (Element*)malloc(sizeof(Element));
	Element* tmpValue = (Element*)malloc(sizeof(Element));
	tmpLeft = (*list).head;
	tmp = (*tmpLeft).next;
	tmpRight = (*tmp).next;

	for (int i = 1; i < (*list).leng; i++)
	{
		for (int j = i + 1; j < (*list).leng; j++)
		{
			if (tmp->value > tmpRight->value)
			{
				tmp = tmpRight;
				tmpRight = tmpRight->next;
			}
			else tmpRight = tmpRight->next;
		}
		if (tmp->value < tmpLeft->value)
		{
			tmpValue->value = tmpLeft->value;
			tmpLeft->value = tmp->value;
			tmp->value = tmpValue->value;
		}
		if (i < (*list).leng - 1)
		{
			tmpLeft = tmpLeft->next;
			tmp = tmpLeft->next;
			tmpRight = tmp->next;
		}
	}

}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	int n = 20;
	int value = -50 + rand() % 101;
	List* list = newList();
	newHead(list, &value);
	value = -50 + rand() % 101;
	newTail(list, &value);
	for (int i = 1; i < n; i++)
	{
		value = -50 + rand() % 101;
		newElem(list, &value, i);
	}
	printList(list);
	auto begin = std::chrono::steady_clock::now();
	SelectSort(list);
	auto end = std::chrono::steady_clock::now();
	auto resultTime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
	printf("\nВідсортовано!\n\n");
	printList(list);
	printf("\nЧас, витрачений на сортування: %lld ns\n", resultTime.count());
	return 0;
}