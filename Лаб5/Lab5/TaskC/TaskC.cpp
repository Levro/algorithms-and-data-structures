﻿#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <chrono>
struct Element
{
	int* value;
	struct Element* next;
	struct Element* previous;
};
struct List
{
	struct Element* head;
	struct Element* tail;
	size_t leng;
};
List* newList(void) {
	List* list = (List*)malloc(sizeof(List));
	if (list) {
		(*list).leng = 0;
		(*list).head = (*list).tail = NULL;
	}
	return list;
}
bool ListCheck(List* list)
{
	return (((*list).head == NULL) || ((*list).tail == NULL));
}
void printList(List* list)
{
	Element* out = (Element*)malloc(sizeof(Element));
	out = (*list).head;
	printf("Список:\n[ _ ");
	for (int i = 0; i < (*list).leng; i++)
	{
		printf("%d _ ", *(out->value));
		out = (*out).next;
	}
	printf("]\n");
}
void newHead(List* list, int& data)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = &data;
	(*elem).next = (*list).head;
	(*elem).previous = NULL;
	if (!ListCheck(list))
		(*(*list).head).previous = elem;
	else
		(*list).tail = elem;
	(*list).head = elem;
	(*list).leng++;
}
void newTail(List* list, int& data)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = &data;
	(*(*list).tail).next = elem;
	(*list).tail = elem;
	(*list).leng++;
}
void newElem(List* list, int& data, int index)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = &data;
	Element* tmp = (Element*)malloc(sizeof(Element));
	tmp = (*list).head;
	for (int i = 1; i < index; i++)
		tmp = (*tmp).next;
	(*elem).next = (*tmp).next;
	(*tmp).next = elem;
	(*list).leng++;
}
void insertSort(int arr[], int n)
{
	int с;
	for (int i = 0; i < n; i++)
	{
		с = arr[i];
		for (int j = i - 1; j >= 0 && arr[j] > с; j--)
		{
			arr[j + 1] = arr[j];
			arr[j] = с;
		}
	}

}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	List* list = newList();
	int n = 10;
	int arr[10];
	int* arrPoint = &arr[0];
	newHead(list, arrPoint[0]);
	arr[0] = -50 + rand() % 101;
	arr[n - 1] = -50 + rand() % 101;
	newTail(list, arrPoint[5]);
	for (int i = 1; i < n - 1; i++)
	{
		arr[i] = -50 + rand() % 101;
		newElem(list, arrPoint[i], i);
	}
	printList(list);
	auto begin = std::chrono::steady_clock::now();
	insertSort(arr, n);
	auto end = std::chrono::steady_clock::now();
	auto resultTime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
	printf("\nВідсортовано!\n\n");
	printList(list);
	printf("\nЧас, витрачений на сортування: %lld ns\n", resultTime.count());
	return 0;
}