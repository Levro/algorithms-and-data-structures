﻿#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <chrono>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));
	int n = 10;
	int arr[10];
	printf("[ ");
	for (int i = 0; i < n; i++)
	{
		arr[i] = -50 + rand() % 101;
		printf("%d ", arr[i]);
	}
	printf(" ]\n\nВідсортовано!");
	int с;
	auto begin = std::chrono::steady_clock::now();
	for (int i = 0; i < n; i++)
	{
		с = arr[i];
		for (int j = i - 1; j >= 0 && arr[j] > с; j--)
		{
			arr[j + 1] = arr[j];
			arr[j] = с;
		}
	}
	auto end = std::chrono::steady_clock::now();
	auto resultTime = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
	printf("\n\n[ ");
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("]\nЧас, витрачений на сортування: %lld ns\n", resultTime.count());
	return 0;
}
