﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            int City = 0, i = City;
            int[] Distance = new int[19];
            int[,] matrix = new int[19, 19];

            string ResultOut = "";
            string[] Cities = new string[19] { "Київ", "Житомир",  "Новоград-Волинський", "Рівне", "Луцьк", "Бердичів",  "Вінниця",
            "Хмельницький", "Тернопіль", "Шепетівка", "Біла церква", "Умань", "Черкаси", "Кременчуг", "Полтава", "Харків",  "Прилуки",  "Суми", "Миргород" };
            matrix[0, 1] = 135;
            matrix[0, 10] = 78;
            matrix[0, 16] = 128;
            matrix[1, 2] = 80;
            matrix[1, 5] = 38;
            matrix[1, 9] = 115;
            matrix[2, 3] = 100;
            matrix[3, 4] = 68;
            matrix[5, 6] = 73;
            matrix[6, 7] = 110;
            matrix[7, 8] = 104;
            matrix[10, 11] = 115;
            matrix[10, 12] = 146;
            matrix[10, 14] = 181;
            matrix[12, 13] = 105;
            matrix[14, 15] = 130;
            matrix[16, 17] = 175;
            matrix[16, 18] = 109;

            var Queue = new Queue<int>(); 
            Queue.Enqueue(City); 
            bool[] IsVisited = new bool[19];
            IsVisited[City] = true;


            Console.WriteLine("Алгоритм пошуку DFS:");
            DepthFirstSearch(Cities, matrix, City, ResultOut, City, 0);

            Console.WriteLine("<-------------------------------------------------------->");
            City = 0;
            Console.WriteLine("Алгоритм пошуку BFS:");
            while (Queue.Count != 0)
            {
                City = Queue.Dequeue(); 
                i = City;  
                while (i < 19) 
                {
                    if (!IsVisited[i] && Convert.ToBoolean(matrix[City, i])) 
                    {
                        IsVisited[i] = true; 
                        Queue.Enqueue(i); 
                        Distance[i] = Distance[City] + matrix[City, i]; 
                        Console.WriteLine($": Київ -> {Cities[i]} - {Distance[i]}км");
                    }
                    i++;
                }
            }
            Console.ReadKey();
        }
        private static void DepthFirstSearch(string[] cities, int[,] matrix, int city, string resultOut, int cityOut, int distance)
        {
            if (city != 0) 
            {
                resultOut = $"{resultOut} -> {matrix[cityOut, city]}км -> {cities[city]} "; 
                distance += matrix[cityOut, city]; 
                Console.WriteLine($"{resultOut} | {distance}км |");
            }
            else resultOut = $"{cities[city]} ";
            for (int i = 0; i < 19; i++)
            {
                if (Convert.ToBoolean(matrix[city, i]))
                {
                    DepthFirstSearch(cities, matrix, i, resultOut, city, distance);

                }
            }
        }
    }
}