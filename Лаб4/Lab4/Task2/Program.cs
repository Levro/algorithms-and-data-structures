﻿using System;
using System.Collections.Generic;

namespace Task_3_cSharp
{
    class Program
    {
        public static bool isSpace(char symb)
        {
            if (symb != ' ') return false;
            else return true;
        }
        public static bool isAction(char symb)
        {
            string act = "+-*/^S";
            for (int i = 0; i < 6; i++)
                if (symb == act[i])
                    return true;
            return false;
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;
            Stack<double> myStack = new Stack<double>();
            double check; string tempString;
            Console.WriteLine("Введіть приклад у зворотному польському записі: "); tempString = Console.ReadLine();
            for (int i = 0; i < tempString.Length; i++)
            {
                if (isAction(tempString[i]))
                {
                    double right;
                    double left;
                    if (tempString[i] == '*')
                    {
                        right = myStack.Pop();
                        left = myStack.Pop();
                        myStack.Push(left * right);
                    }
                    else if (tempString[i] == '/')
                    {
                        right = myStack.Pop();
                        left = myStack.Pop();
                        myStack.Push(left / right);
                    }
                    else if (tempString[i] == '^')
                    {
                        right = myStack.Pop();
                        left = myStack.Pop();
                        myStack.Push(Math.Pow(left, right));
                    }
                    else if ((tempString[i] == 'S') || (tempString[i] == 's'))
                    {
                        right = myStack.Pop();
                        myStack.Push(Math.Sqrt(right));
                    }
                    else if (tempString[i] == '+')
                    {
                        right = myStack.Pop();
                        left = myStack.Pop();
                        myStack.Push(left + right);
                    }
                    else if (tempString[i] == '-')
                    {
                        right = myStack.Pop();
                        left = myStack.Pop();
                        myStack.Push(left - right);
                    }
                }
                else
                {
                    string valuesString = null;
                    while (!isSpace(tempString[i]) && !isAction(tempString[i]) && tempString[i] != '(' && tempString[i] != ')')
                    {
                        valuesString += tempString[i];
                        i++;
                        if (i == tempString.Length) break;
                    }
                    if (double.TryParse(valuesString, out check))
                        myStack.Push(double.Parse(valuesString));
                }
            }
            Console.WriteLine($"Отриманий результат: {myStack.Peek()}");
        }
    }
}