﻿#include <stdio.h>
#include <windows.h>

struct Element
{
	int value;
	struct Element* next;
	struct Element* previous;
};
struct List
{
	struct Element* head;
	struct Element* tail;
	size_t leng;
};

List* newList(void) {
	List* list = (List*)malloc(sizeof(List));
	if (list) {
		(*list).leng = 0;
		(*list).head = (*list).tail = NULL;
	}
	return list;
}
bool ListCheck(List* list)
{
	return (((*list).head == NULL) || ((*list).tail == NULL));
}

void newHead(List* list, int* data)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = *data;
	(*elem).next = (*list).head;
	(*elem).previous = NULL;
	if (!ListCheck(list))
		(*(*list).head).previous = elem;
	else
		(*list).tail = elem;
	(*list).head = elem;
	(*list).leng++;
}
void newTail(List* list, int* data)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = *data;
	(*(*list).tail).next = elem;
	(*list).tail = elem;
	(*list).leng++;
}
void newElem(List* list, int* data, int index)
{
	Element* elem = (Element*)malloc(sizeof(Element));
	(*elem).value = *data;
	Element* tmp = (Element*)malloc(sizeof(Element));
	tmp = (*list).head;
	for (int i = 1; i < index; i++)
		tmp = (*tmp).next;
	(*elem).next = (*tmp).next;
	(*tmp).next = elem;
	(*list).leng++;
}

void deleteHead(List* list)
{
	Element* delElem = (Element*)malloc(sizeof(Element));
	delElem = (*list).head;
	(*list).head = (*list).head->next;
	free(delElem);
	(*list).leng--;
}
void deleteTail(List* list)
{
	Element* delElem = (Element*)malloc(sizeof(Element));
	delElem = (*list).tail;
	(*list).tail = (*list).tail->previous;
	free(delElem);
	(*list).leng--;
}
void deleteElem(List* list, int index)
{
	Element* delElem = (Element*)malloc(sizeof(Element));
	Element* tmp = (Element*)malloc(sizeof(Element));
	tmp = (*list).head;
	for (int i = 0; i < index; i++)
		tmp = (*tmp).next;
	delElem = (*tmp).next;
	(*tmp).next = (*delElem).next;
	free(delElem);
	(*list).leng--;
}

void deleteList(List* list)
{
	Element* head = (*list).head;
	Element* next = NULL;
	while (head) {
		next = (*head).next;
		free(head);
		head = next;
	}
	free(list);
	list = NULL;
}
void printList(List* list)
{
	Element* out = (Element*)malloc(sizeof(Element));
	out = (*list).head;
	printf("Список:\n[ - ");
	for (int i = 0; i < (*list).leng; i++)
	{
		printf("%d - ", (*out).value);
		out = (*out).next;
	}
	printf("]\n");
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int menu;
	int value = 0;
	List* list = NULL;
	do {
		system("cls");
		printf("1. Стоврити список\n");
		printf("2. Додати вузол\n");
		printf("3. Видалити вузол\n");
		printf("4. Вивести список\n");
		printf("5. Видалити список\n");
		printf("6. Вихід з програми\n");
		printf(":");
		scanf_s("%d", &menu);
		switch (menu)
		{
		case 1:
		{
			system("cls");
			list = newList();
			printf("Список створено.\n");
			system("pause");
		}break;
		case 2:
		{
			if (list == NULL)
			{
				system("cls");
				printf("Спочатку створіть список!\n");
				system("pause");
				break;
			}
			system("cls");
			printf("1. Додати вузол на початок списку\n");
			printf("2. Додати вузол на кінець списку\n");
			printf("3. Додати вузол в довільну позицію списку\n");
			printf(":");
			scanf_s("%d", &menu);
			if (menu == 1)
			{
				printf("Введіть значення для вузла:\n: ");
				scanf_s("%d", &value);
				newHead(list, &value);
			}
			else if (menu == 2)
			{
				printf("Введіть значення для вузла:\n: ");
				scanf_s("%d", &value);
				newTail(list, &value);
			}
			else if (menu == 3)
			{
				printf("Оберіть позицію для нового вузла (0 ... %d):\n ", (*list).leng);
				scanf_s("%d", &menu);
				printf("Введіть значення для вузла:\n");
				scanf_s("%d", &value);
				if (menu == 0)
					newHead(list, &value);
				else if (menu == (*list).leng - 1)
					newTail(list, &value);
				else
					newElem(list, &value, menu);
			}
			else
				printf("Помилка введення!\n");
			system("pause");
		}break;
		case 3:
		{
			system("cls");
			printf("1. Видалити перший вузол в списку\n");
			printf("2. Видалити останній вузол\n");
			printf("3. Видалити вузол у довільний позиції\n");
			printf(":");
			scanf_s("%d", &menu);
			if (menu == 1)
				deleteHead(list);
			else if (menu == 2)
				deleteTail(list);
			else if (menu == 3)
			{
				printf("Оберіть який вузол видалити (0 ... %d):\n ", (*list).leng);
				scanf_s("%d", &menu);
				deleteElem(list, menu);
			}
			else
				printf("Помилка введення!\n");
			system("pause");
		}break;
		case 4:
		{
			if (list == NULL) {
				printf("Список не існує.\n");
			}
			else printList(list);
			system("pause");
		}break;
		case 5:
		{
			if (list == NULL)
			{
				system("cls");
				printf("Спочатку створіть список!\n");
				system("pause");
				break;
			}
			deleteList(list);
			printf("Список видалений.\n");
			system("pause");
		}break;
		case 6: return 0;
		default:
			printf("Помилка при виборі пункта меню.\n");
			system("pause");
		}
	} while (true);
	return 0;
}